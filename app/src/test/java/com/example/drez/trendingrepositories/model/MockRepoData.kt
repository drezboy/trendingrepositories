package com.example.drez.trendingrepositories.model

import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import org.junit.Test

class MockRepoData {

    fun createRepoItem(items: MutableList<RepoData> = mutableListOf(createRepoData())) = RepoItems(items = items)

    fun createRepoData(
            title: String = "title",
            description: String = "description",
            stargazers_count: String = "1000",
            owner: RepoOwner = createRepoOwner(),
            forks: String = "5",
            watchers: String = "10",
            language: String = "Kotlin, Java"
    ) = RepoData(
            title = title,
            description = description,
            stargazers_count = stargazers_count,
            owner = owner,
            forks = forks,
            watchers = watchers,
            language = language)

    fun createRepoOwner(avatar: String = "avatar_url", login: String = "login") = RepoOwner(avatar = avatar, login = login)


    @Test
    fun `create repo owner with one argument`() {
        val repoOwner = mock<RepoOwner> {
            on { it.avatar } doReturn ("avatar_url")
        }
        whenever(repoOwner.avatar).thenReturn("avatar_url")
    }
}
