package com.example.drez.trendingrepositories

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.example.drez.trendingrepositories.adapter.ListAdapter
import com.example.drez.trendingrepositories.model.RepoData
import com.example.drez.trendingrepositories.model.State
import com.example.drez.trendingrepositories.viewmodel.RepoViewModel
import kotlinx.android.synthetic.main.repos_list_fragment.*

class TrendingListFragment : Fragment() {

    companion object {
        fun newInstance(): TrendingListFragment {
            return TrendingListFragment()
        }
    }
    private lateinit var adapter: ListAdapter
    private lateinit var listener: OnRepoSelectedListener
    private lateinit var model: RepoViewModel


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.repos_list_fragment, container, false)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnRepoSelectedListener) {
            listener = context
        } else {
            throw ClassCastException(context.toString() + " must implement OnRepoSelectedListener.")
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        model = activity?.run {
            ViewModelProviders.of(this).get(RepoViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
        adapter = ListAdapter(requireContext())
        adapter.setListener(listener)
        repos_list.layoutManager = LinearLayoutManager(activity, LinearLayout.VERTICAL, false)
        repos_list.adapter = adapter
        fetchData()
        initState()
    }

    private fun fetchData() {
        model.listOfTrendingRepos.observe(this, Observer {

            adapter.submitList(it)
        })
    }

    private fun initState() {
        error_view.setOnClickListener { model.retry() }
        model.getState().observe(this, Observer { state ->
            progress_bar.visibility = if (model.listIsEmpty() && state == State.LOADING) View.VISIBLE else View.GONE
            error_view.visibility = if (model.listIsEmpty() && state == State.ERROR) View.VISIBLE else View.GONE
            if (!model.listIsEmpty()) {
                adapter.setState(state ?: State.DONE)
            }
        })
    }

    /**
     * Interface which should be used to connect master and details fragment. Provides one function, onRepoSelected
     */
    interface OnRepoSelectedListener {
        /**
         *  Once the item from the master fragment has been selected, this function informs parent activity about it
         *
         * @param[repoData] data class which represents one repository
         */
        fun onRepoSelected(repoData: RepoData)
    }
}