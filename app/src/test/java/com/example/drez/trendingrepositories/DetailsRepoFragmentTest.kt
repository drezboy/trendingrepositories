package com.example.drez.trendingrepositories

import android.os.Build
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(constants = BuildConfig::class, sdk = [Build.VERSION_CODES.O_MR1])
class DetailsRepoFragmentTest {

    @Test
    fun shouldNotBeNull() {
        val fragment = DetailsRepoFragment.newInstance()
        Assert.assertNotNull(fragment)
    }
}
