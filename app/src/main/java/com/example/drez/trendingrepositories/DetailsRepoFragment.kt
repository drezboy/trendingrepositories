package com.example.drez.trendingrepositories

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.example.drez.trendingrepositories.model.RepoData
import com.example.drez.trendingrepositories.viewmodel.RepoViewModel

class DetailsRepoFragment : Fragment() {
    companion object {
        fun newInstance(): DetailsRepoFragment {
            return DetailsRepoFragment()
        }
    }

    private lateinit var model: RepoViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.repo_details_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        model = activity?.run {
            ViewModelProviders.of(this).get(RepoViewModel::class.java)
        } ?: throw Exception("Invalid Activity")

        model.selected.observe(this, Observer { response -> response?.let { consumeResponse(it) } })
    }

    private fun consumeResponse(repoData: RepoData) {

        val avatar = view!!.findViewById<ImageView>(R.id.avatar_details)
        Glide.with(view!!).load(repoData.owner.avatar).into(avatar)

        val name = view?.findViewById<TextView>(R.id.tv_repo_name)
        name?.text = repoData.title
        val description = view?.findViewById<TextView>(R.id.tv_repo_description)
        description?.text = repoData.description
        val language = view?.findViewById<TextView>(R.id.tv_language)
        language?.text = repoData.language
        val watchers = view?.findViewById<TextView>(R.id.tv_watchers)
        watchers?.text = repoData.watchers
        val forks = view?.findViewById<TextView>(R.id.tv_forks)
        forks?.text = repoData.forks
    }

}