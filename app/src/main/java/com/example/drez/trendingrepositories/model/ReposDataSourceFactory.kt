package com.example.drez.trendingrepositories.model

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.DataSource

class ReposDataSourceFactory(githubApi: GithubApi) : DataSource.Factory<Int, RepoData>() {

    val repoDataSourceLiveData = MutableLiveData<RepositoryDataSource>()
    private val repoDataSource = RepositoryDataSource(githubApi)

    override fun create(): DataSource<Int, RepoData> {
        repoDataSourceLiveData.postValue(repoDataSource)
        return repoDataSource
    }
}
