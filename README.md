# TrendingRepositories

This project is intended to list trending repositories from GitHub.

## Architecture

Model - View - ViewModel architectural pattern is used with Android Architecture Components collection of libraries (Paging library, LiveData, ViewModel).

## Built With
* [Gradle] (https://gradle.org/) - Dependency Management

## License

This project is licensed under the MIT License
