package com.example.drez.trendingrepositories.adapter

import android.arch.paging.PagedListAdapter
import android.content.Context
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.example.drez.trendingrepositories.R
import com.example.drez.trendingrepositories.model.State
import com.example.drez.trendingrepositories.TrendingListFragment
import com.example.drez.trendingrepositories.model.RepoData
import kotlinx.android.synthetic.main.item_list_footer.view.*

class ListAdapter(private val context: Context) : PagedListAdapter<RepoData, RecyclerView.ViewHolder>(EnhancedRepoDataDiffCallback) {

    private val data = 1
    private val footer = 2
    private var state = State.LOADING

    private lateinit var firelistener: TrendingListFragment.OnRepoSelectedListener
    private var onClickListener: View.OnClickListener

    init {
        onClickListener = View.OnClickListener { v ->
            val item = v.tag as RepoData
            firelistener.onRepoSelected(item)
        }
    }

    companion object {
        const val TAG = "ListAdapter"
        val EnhancedRepoDataDiffCallback = object : DiffUtil.ItemCallback<RepoData>() {
            override fun areItemsTheSame(oldItem: RepoData, newItem: RepoData): Boolean {
                Log.d(TAG, "same: " + (oldItem.title == newItem.title))
                return oldItem.title == newItem.title
            }
            override fun areContentsTheSame(oldItem: RepoData, newItem: RepoData): Boolean {
                Log.d(TAG, "same contents: " + (oldItem == newItem))
                return oldItem == newItem
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == data) {
            ViewHolder(LayoutInflater.from(context).inflate(R.layout.repo_item, parent, false))
        } else {
            ListFooterViewHolder(LayoutInflater.from(context).inflate(R.layout.item_list_footer, parent, false))
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position) == data) {
            (holder as ViewHolder).bindItems(getItem(position))
            with(holder.itemView) {
                tag = getItem(position)
                setOnClickListener(onClickListener)
            }
        } else {
            (holder as ListFooterViewHolder).bind(state)
        }
    }

    override fun getItemCount(): Int {
        return super.getItemCount() + if (hasFooter()) 1 else 0
    }

    override fun getItemViewType(position: Int): Int {
        return if (position < super.getItemCount()) data else footer
    }

    private fun hasFooter(): Boolean {
        return super.getItemCount() != 0 && (state == State.LOADING || state == State.ERROR)
    }

    fun setListener(listener: TrendingListFragment.OnRepoSelectedListener) {
       firelistener = listener
    }

    fun setState(state: State) {
        this.state = state
        notifyItemChanged(super.getItemCount())
    }
}

class ViewHolder (val view: View) : RecyclerView.ViewHolder(view) {
    fun bindItems(repoData: RepoData?) {
        val title = view.findViewById<TextView>(R.id.title)
        title.text = repoData?.title

        val description = view.findViewById<TextView>(R.id.description)
        description.text = repoData?.description

        val stargazersCount = view.findViewById<TextView>(R.id.count_of_stars)
        stargazersCount.text = repoData?.stargazers_count

        val avatar = view.findViewById<ImageView>(R.id.avatar)

//        Picasso.get().load(repoData.owner.avatar).resize(200, 200).centerCrop().onlyScaleDown().into(avatar)
        Glide.with(view).load(repoData?.owner?.avatar).into(avatar)
    }
}

class ListFooterViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
    fun bind(status: State?) {
        view.progress_bar_footer.visibility = if (status == State.LOADING) VISIBLE else View.INVISIBLE
        view.txt_error.visibility = if (status == State.ERROR) VISIBLE else View.INVISIBLE
    }
}
