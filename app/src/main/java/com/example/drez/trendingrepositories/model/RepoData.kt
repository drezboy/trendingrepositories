package com.example.drez.trendingrepositories.model

import com.squareup.moshi.Json

/**
 * Data class which represents single repository item
 */
data class RepoData(@field:Json(name = "name") var title: String,
                    @field:Json(name = "description") var description: String,
                    @field:Json(name = "stargazers_count") var stargazers_count: String,
                    @field:Json(name = "owner") var owner: RepoOwner,
                    @field:Json(name = "forks_count") var forks: String,
                    @field:Json(name = "watchers") var watchers: String,
                    @field:Json(name = "language") var language: String)

/**
 * Data class which represents owner of the repository
 */
data class RepoOwner(@field:Json(name = "avatar_url") var avatar: String,
                     @field:Json(name = "login") var login: String)

/**
 * Data class which represent list of the repositories
 */
data class RepoItems(@field:Json(name = "items") var items:  MutableList<RepoData>)
