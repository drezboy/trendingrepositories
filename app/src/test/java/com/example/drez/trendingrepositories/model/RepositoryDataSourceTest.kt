package com.example.drez.trendingrepositories.model

import com.nhaarman.mockito_kotlin.*
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.junit.After
import org.junit.Assert
import org.junit.Before

import org.junit.Test
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.ArgumentMatchers.anyString
import retrofit2.Call
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*

class RepositoryDataSourceTest {
    private lateinit var mockGithubApi: GithubApi
    private lateinit var callMock: Call<RepoItems>
    private lateinit var repositoryDataSourceMock: RepositoryDataSource
    private lateinit var repositoryDataSourceSpy: RepositoryDataSource

    @Before
    fun setUp() {
        mockGithubApi = mock()
        callMock = mock()
        repositoryDataSourceMock = mock()
        repositoryDataSourceSpy = spy(RepositoryDataSource(mockGithubApi))

        whenever(mockGithubApi.getTrendingRepos(anyInt(), anyInt(), anyString())).thenReturn(callMock)
    }

    @After
    fun tearDown() {
    }

    @Test
    fun `getTrendingRepos test success`() {
        // prepare data
        val mockRepoData = MockRepoData()
        val repoOwner = mockRepoData.createRepoOwner(login = "userName1")
        val repoData = mockRepoData.createRepoData(title = "repo1", owner = repoOwner)
        val repoData2 = mockRepoData.createRepoData(title = "repo2", owner = repoOwner)
        val repoData3 = mockRepoData.createRepoData(title = "repo3")
        val repoItems = mockRepoData.createRepoItem(mutableListOf(repoData, repoData2, repoData3))
        val response = Response.success(repoItems)
        whenever(callMock.execute()).thenReturn(response)

        // call
        val realResponse = mockGithubApi.getTrendingRepos(anyInt(), anyInt(), anyString()).execute()

        // assert
        Assert.assertNotNull(realResponse)
    }

    @Test
    fun `getTrendingRepos test size`() {
        // prepare data
        val mockRepoData = MockRepoData()
        val repoOwner = mockRepoData.createRepoOwner(login = "userName1")
        val repoData = mockRepoData.createRepoData(title = "repo1", owner = repoOwner)
        val repoData2 = mockRepoData.createRepoData(title = "repo2", owner = repoOwner)
        val repoData3 = mockRepoData.createRepoData(title = "repo3")
        val repoItems = mockRepoData.createRepoItem(mutableListOf(repoData, repoData2, repoData3))
        val response = Response.success(repoItems)
        whenever(callMock.execute()).thenReturn(response)

        // call
        val realResponse = mockGithubApi.getTrendingRepos(anyInt(), anyInt(), anyString()).execute()

        // assert
        Assert.assertEquals(realResponse.body()?.items?.size, response.body()?.items?.size)
    }

    @Test
    fun `getTrendingRepos test error`() {
        // prepare
        val responseError = Response.error<RepoItems>(422, ResponseBody.create(MediaType.parse("application/json"), "Unprocessable Entity"))
        whenever(callMock.execute()).thenReturn(responseError)

        // call
        val realResponse = mockGithubApi.getTrendingRepos(anyInt(), anyInt(), anyString()).execute()

        // assert
        Assert.assertNotNull(realResponse.errorBody())
    }

    @Test
    fun `getCreatedDaysAgo for specific date`() {
        repositoryDataSourceMock.getCreatedDaysAgo(Int.MIN_VALUE)
        repositoryDataSourceMock.getCreatedDaysAgo(Int.MAX_VALUE)
        repositoryDataSourceMock.getCreatedDaysAgo(anyInt())

        verify(repositoryDataSourceMock).getCreatedDaysAgo(Int.MIN_VALUE)
        verify(repositoryDataSourceMock).getCreatedDaysAgo(Int.MAX_VALUE)
    }

    @Test
    fun `get created days`() {
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.DAY_OF_YEAR, -5)
        val format = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
        Assert.assertEquals("created:>" + format.format(calendar.time), repositoryDataSourceSpy.getCreatedDaysAgo(5))
    }
}