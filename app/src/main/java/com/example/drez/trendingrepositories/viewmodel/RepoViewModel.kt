package com.example.drez.trendingrepositories.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import com.example.drez.trendingrepositories.model.*
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

class RepoViewModel : ViewModel() {
    val selected = MutableLiveData<RepoData>()
    private val pageSize = 50

    var listOfTrendingRepos: LiveData<PagedList<RepoData>>
    var reposDataSourceFactory: ReposDataSourceFactory
    private var repository: RepositoryDataSource

    private val githubApi by lazy {
        create()
    }

    companion object {
        private var BASE_URL = "https://api.github.com/"
        fun create(): GithubApi {
            val retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(MoshiConverterFactory.create())
                    .build()
            return retrofit.create(GithubApi::class.java)
        }
    }

    init {
        reposDataSourceFactory = ReposDataSourceFactory(githubApi)
        repository = RepositoryDataSource(githubApi)
        val config = PagedList.Config.Builder()
                .setPageSize(pageSize)
                .setInitialLoadSizeHint(pageSize * 2)
                .setEnablePlaceholders(false)
                .build()
        listOfTrendingRepos = LivePagedListBuilder<Int, RepoData>(reposDataSourceFactory, config)
                .build()
    }

    fun getState(): LiveData<State> = Transformations.switchMap<RepositoryDataSource, State>(reposDataSourceFactory.repoDataSourceLiveData,
            RepositoryDataSource::state)

    fun retry() {
        reposDataSourceFactory.repoDataSourceLiveData.value?.retry()
    }

    fun listIsEmpty(): Boolean {
        return listOfTrendingRepos.value?.isEmpty() ?: true
    }

    fun setSelectedRepo(repoData: RepoData) {
        selected.value = repoData
    }
}


