package com.example.drez.trendingrepositories

import android.os.Build
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(constants = BuildConfig::class, sdk = [Build.VERSION_CODES.O_MR1])
class MainActivityTest {

    lateinit var mainActivity: MainActivity

    @Before
    fun setUp() {
        mainActivity = Robolectric.setupActivity(MainActivity::class.java)
    }

    @Test
    fun shouldNotBeNull() {
        Assert.assertNotNull(mainActivity)
    }

    @Test
    fun shouldHaveMasterFragment() {
        Assert.assertNotNull(mainActivity.supportFragmentManager.findFragmentByTag("trendingFragment"))
    }

    @Test
    fun shouldNotHaveDetailsFragmentAtStart() {
        Assert.assertNull(mainActivity.supportFragmentManager.findFragmentByTag("detailsRepoFragment"))
    }

    @Test
    fun onCreate() {
    }

    @Test
    fun onRepoSelected() {
    }
}
