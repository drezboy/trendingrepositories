package com.example.drez.trendingrepositories

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.example.drez.trendingrepositories.model.RepoData
import com.example.drez.trendingrepositories.viewmodel.RepoViewModel

class MainActivity : AppCompatActivity(), TrendingListFragment.OnRepoSelectedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager
                .beginTransaction()
                .add(R.id.container, TrendingListFragment.newInstance(), "trendingFragment")
                .commit()
        }
    }

    override fun onRepoSelected(repoData: RepoData) {
        Toast.makeText(this, "Selected " + repoData.title + "!", Toast.LENGTH_SHORT).show()

        val model = run {
            ViewModelProviders.of(this).get(RepoViewModel::class.java)
        }
        model.setSelectedRepo(repoData)

        val detailsRepoFragment = DetailsRepoFragment.newInstance()
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, detailsRepoFragment, "detailsRepoFragment")
        transaction.addToBackStack(null)
        transaction.commit()
    }
}