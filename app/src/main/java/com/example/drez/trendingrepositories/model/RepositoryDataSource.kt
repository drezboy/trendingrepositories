/*
 * Repository modules handle data operations. They provide a clean API so that the rest of the app can retrieve this data easily.
 * They know where to get the data from and what API calls to make when data is updated.
 * You can consider repositories to be mediators between different data sources, such as persistent
 * models, web services, and caches.
 * */

package com.example.drez.trendingrepositories.model

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.PageKeyedDataSource
import android.util.Log
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*

class RepositoryDataSource(var githubApi: GithubApi) : PageKeyedDataSource<Int, RepoData>() {


    var state: MutableLiveData<State> = MutableLiveData()
    var date = 7

    companion object {
        const val TAG = "RepositoryDataSource"
    }

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, RepoData>) {
        updateState(State.LOADING)
        githubApi.getTrendingRepos(1, params.requestedLoadSize, getCreatedDaysAgo(date)).enqueue(object : Callback<RepoItems> {
            override fun onResponse(call: Call<RepoItems>, response: Response<RepoItems>) {
                if (response.isSuccessful) {
                    updateState(State.DONE)
                    response.body()?.let {
                        Log.d(TAG, "loadInitial size: " + it.items.size)
                        callback.onResult(it.items, null, 2)
                    }
                } else {
                    Log.d(TAG, "loadInitial size: isSuccessful")
                    updateState(State.ERROR)
                }
            }

            override fun onFailure(call: Call<RepoItems>, t: Throwable) {
                Log.d(TAG, "loadInitial size: onFailure")
                updateState(State.ERROR)
            }
        })
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, RepoData>) {
        updateState(State.LOADING)
        githubApi.getTrendingRepos(params.key, params.requestedLoadSize, getCreatedDaysAgo(date)).enqueue(object : Callback<RepoItems> {
            override fun onResponse(call: Call<RepoItems>, response: Response<RepoItems>) {
                if (response.isSuccessful) {
                    updateState(State.DONE)
                    response.body()?.let {
                        Log.d(TAG, "loadAfter, size" + it.items.size)
                        callback.onResult(it.items, params.key + 1)
                    }
                } else {
                    updateState(State.ERROR)
                }
            }

            override fun onFailure(call: Call<RepoItems>, t: Throwable) {
                updateState(State.ERROR)
            }
        })
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, RepoData>) {
        // not needed
    }

    /**
     * Formats the string with given number of days in format created:>yyyy-MM-dd
     *
     * @param[daysAgo] represents information about wanted time interval
     * @return the formatted string
     */
    fun getCreatedDaysAgo(daysAgo: Int): String {
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.DAY_OF_YEAR, -daysAgo)
        val format = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
        return "created:>" + format.format(calendar.time)
    }

    fun retry() {

    }

    /**
     * Updates the current processing data state
     *
     * @param[state] @see com.example.drez.trendingrepositories.model.State
     */
    private fun updateState(state: State) {
        this.state.postValue(state)
    }
}
