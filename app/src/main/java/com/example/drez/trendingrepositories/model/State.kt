package com.example.drez.trendingrepositories.model

/**
 * State of the processing the data
 */
enum class State {

    /**
     * Data is processed
     */
    DONE,

    /**
     * Data is loading
     */
    LOADING,

    /**
     * Error state, either network or bad request
     */
    ERROR
}
