package com.example.drez.trendingrepositories.model

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Interface which represents queries sent to Github API
 */
interface GithubApi {

    /**
     * Get trending repository with the help of retrofit library. Query should look like this:
     * https://api.github.com/search/repositories?q=created:>2018-10-07&sort=stars&order=desc
     *
     * @param[page] number of the page which should be queried
     * @param[perPage] number of repositories per page
     * @param[q] The search terms. In this case, date of creation of repositories
     *
     * @return retrofit Call object
     */
    @GET("search/repositories?sort=stars&order=desc")
    fun getTrendingRepos(@Query("page") page: Int, @Query("per_page") perPage: Int, @Query("q") date: String) : Call<RepoItems>

}
